
module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/my-storage-room/'
      : '/'
  }